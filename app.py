from flask import Flask, make_response, render_template, request

from Contexter import organic_caller
from ContextRepository import ContextRepository
from Contexter.Objects import Organic
from Contexter.Siter import Siter

app = Flask(__name__, template_folder='./templates', static_folder='./statics')


@app.route('/')
def main():
    q = request.args.get('q', default=None)
    bc = request.args.get('bc', default=None)

    if bc:
        bc = bc.split('|||')
    else:
        bc = [q]

    if q:
        o = Organic(organic_caller(q, 10))
        c = Siter.concatenator(*Siter.org_siter(q, o))
        context_repo = ContextRepository(c)
        return render_template('home.html', o=Organic(organic_caller(q, 10)), cr=context_repo, q=q, bc=bc)
    else:
        return render_template('home.html', o=None)
    pass


@app.route('/how')
def how():
    return render_template('how.html')


if __name__ == '__main__':
    app.run(port=5000, host='0.0.0.0', debug=True)
    pass
