import requests

HEADERS = {'Authorization': 'Basic aGFja2F0aG9uOkFoSjR4aWU2bGllME9wYXU=', 'Content-Type': 'application/json'}


def caller_graphql(query, variables=None):
    req = requests.post('https://cqc.seznam.net/hackathon/graphql', json={'query': query, 'variables': variables if variables else {}}, headers=HEADERS)

    if req.status_code == 200:
        return req.json()
    else:
        raise Exception(f"Něco se rozbilo... HTTP STATUS: {req.status_code}")
    pass


def caller_sug(query, count=10, tokenize=True):
    # https://suggest.seznam.cz/fulltext/cs?phrase=test&format=json-2&count=10
    req = requests.get('https://suggest.seznam.cz/fulltext/cs', params={'phrase': query, 'json': -2, 'count': count, 'highlight': int(tokenize)})
    if req.status_code == 200:
        return req.json()
    else:
        #print(req.content)
        raise Exception(f"Něco se rozbilo... HTTP STATUS: {req.status_code}")
    pass


def listdict_to_object(lst, obj):
    new_list = []
    for item in lst:
        new_list.append(obj(item))
    return new_list