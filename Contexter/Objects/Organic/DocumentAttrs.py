class DocumentAttrs:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def file_type(self) -> str:
        return self.data['fileType']

    @property
    def is_porn(self) -> bool:
        return self.data['isPorn']

    @property
    def is_homepage(self) -> bool:
        return self.data['isHomepage']

    @property
    def language(self) -> str:
        return self.data['language']

    @property
    def last_change_date(self) -> int:
        return self.data['lastChangeDate']

    @property
    def rss_publication_date(self) -> int:
        return self.data['rssPublicationDate']
    pass
