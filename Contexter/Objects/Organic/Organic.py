from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Organic.OrganicDocument import OrganicDocument


class Organic:
    def __init__(self, data: dict):
        self.data = listdict_to_object(data['data']['organic'], OrganicDocument)
        pass

    @property
    def organic_documents(self) -> List[OrganicDocument]:
        return self.data
    pass
