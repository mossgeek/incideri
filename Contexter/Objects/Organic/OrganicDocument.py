from Contexter.Objects.Organic.DocumentSnippet import DocumentSnippet
from Contexter.Objects.Organic.DocumentAttrs import DocumentAttrs


class OrganicDocument:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def doc_id(self) -> str:
        return self.data['docId']

    @property
    def snippet(self) -> DocumentSnippet:
        return DocumentSnippet(self.data['snippet'])

    @property
    def attributes(self) -> DocumentAttrs:
        return DocumentAttrs(self.data['attributes'])
    pass
