class DocumentSnippet:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def url(self) -> str:
        return self.data['url']

    @property
    def description(self) -> str:
        return self.data['description']

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def url_highlighted(self) -> str:
        return self.data['urlHighlighted'] if 'urlHighlighted' in self.data else self.data['url']
    pass
