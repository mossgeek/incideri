from Contexter.Objects.Map.MapMark import MapMark


class MapsResult:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def url(self) -> str:
        return self.data['url']

    @property
    def mark(self) -> MapMark:
        return MapMark(self.data['mark'])
    pass
