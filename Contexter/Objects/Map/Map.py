from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Map.MapsResult import MapsResult


class Map:
    def __init__(self, data):
        self.data = listdict_to_object(data['data']['maps'], MapsResult)
        pass

    @property
    def maps(self) -> List[MapsResult]:
        return self.data
    pass
