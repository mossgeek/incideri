from typing import Optional


class MapMark:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def easting(self) -> Optional[float]:
        return self.data['easting']

    @property
    def lat(self) -> float:
        return self.data['lat']

    @property
    def lon(self) -> float:
        return self.data['lot']

    @property
    def northing(self) -> Optional[float]:
        return self.data['northing']
    pass
