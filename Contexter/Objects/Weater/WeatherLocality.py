class WeatherLocality:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def accusative(self) -> str:
        return self.data['accusative']

    @property
    def region_name(self) -> str:
        return self.data['regionName']

    @property
    def locality_longitude(self) -> float:
        return self.data['localityLongitude']

    @property
    def locality_latitude(self) -> float:
        return self.data['localityLatitude']

    @property
    def locative(self) -> str:
        return self.data['locative']

    @property
    def name(self) -> str:
        return self.data['name']

    @property
    def type(self) -> str:
        return self.data['type']

    @property
    def url(self) -> str:
        return self.data['url']

    @property
    def photo_live(self) -> bool:
        return self.data['photoLive']
    pass
