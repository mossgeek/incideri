from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Weater.WeatherResult import WeatherResult


class Weather:
    def __init__(self, data: dict):
        self.data = data['data']['weather']
        self.data['results'] = listdict_to_object(self.data['results'], WeatherResult)
        pass

    @property
    def locality(self) -> str:
        return self.data["locality"]

    @property
    def request_type(self) -> str:
        return self.data["requestType"]

    @property
    def results(self) -> List[WeatherResult]:
        return self.data['results']
    pass
