from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Weater.WeatherLocality import WeatherLocality
from Contexter.Objects.Weater.WeatherForecast import WeatherForecast


class WeatherResult:
    def __init__(self, data: dict):
        self.data = data
        self.data['nextdaysList'] = listdict_to_object(self.data['nextdaysList'], WeatherForecast)
        pass

    @property
    def locality(self) -> WeatherLocality:
        return WeatherLocality(self.data['locality'])

    @property
    def morning(self) -> WeatherForecast:
        return WeatherForecast(self.data['morgning'])

    @property
    def afternoon(self) -> WeatherForecast:
        return WeatherForecast(self.data['afternoon'])

    @property
    def night(self) -> WeatherForecast:
        return WeatherForecast(self.data['night'])

    @property
    def actual(self) -> WeatherForecast:
        return WeatherForecast(self.data['actual'])

    @property
    def evening(self) -> WeatherForecast:
        return WeatherForecast(self.data['evening'])

    @property
    def next_days_list(self) -> List[WeatherForecast]:
        return self.data['nextdaysList']
    pass
