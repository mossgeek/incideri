class WeatherForecast:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def wind_speed(self) -> int:
        return self.data['windSpeed']

    @property
    def bio(self) -> int:
        return self.data['bio']

    @property
    def temp_night(self) -> int:
        return self.data['tempNight']

    @property
    def wind_direction(self) -> str:
        return self.data['windDirection']

    @property
    def description(self) -> str:
        return self.data['description']

    @property
    def weather_id(self) -> str:
        return self.data['weatherId']

    @property
    def humidity(self) -> int:
        return self.data['humidity']

    @property
    def weather_id_desc(self) -> str:
        return self.data['weatherIdDesc']

    @property
    def temp_day(self) -> int:
        return self.data['tempDay']

    @property
    def cloudiness(self) -> int:
        return self.data['cloudiness']

    @property
    def date(self) -> str:
        return self.data['date']

    @property
    def is_night(self) -> bool:
        return self.data['isNight']

    @property
    def img(self) -> str:
        return self.data['img']
    pass
