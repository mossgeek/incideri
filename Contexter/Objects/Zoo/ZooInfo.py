class ZooInfo:
    def __init__(self, data: dict):
        self.data = data['data']['zoo']
        pass

    @property
    def type(self) -> str:
        return self.data['type']

    @property
    def uri(self) -> str:
        return self.data['uri']

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def extract(self) -> str:
        return self.data['extract']

    @property
    def details(self) -> str:
        return self.data['details']
    pass
