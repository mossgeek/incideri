from typing import List

from Contexter import listdict_to_object
from Contexter.Objects.CQP.CqpLanguage import CqpLanguage
from Contexter.Objects.CQP.CqpQuery import CqpQuery


class CqpMeaning:
    def __init__(self, data: dict):
        self.data = data
        self.data['queries'] = listdict_to_object(self.data['queries'], CqpQuery)
        self.data['languages'] = listdict_to_object(self.data['languages'], CqpLanguage)
        pass

    @property
    def is_porn(self) -> bool:
        p = self.data['isPorn']
        return p == 'yes'

    @property
    def queries(self) -> List[CqpQuery]:
        return self.data['queries']

    @property
    def languages(self) -> List[CqpLanguage]:
        return self.data['languages']
    pass
