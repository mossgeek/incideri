from typing import List, Optional
from Contexter import listdict_to_object
from Contexter.Objects.CQP.CqpLanguage import CqpLanguage


class CqpToken:
    def __init__(self, data: dict):
        self.data = data
        self.data['languages'] = listdict_to_object(self.data['languages'], CqpLanguage) if self.data['languages'] else self.data['languages']
        pass

    @property
    def text(self) -> str:
        return self.data['text']

    @property
    def tokenizer_charclass(self) -> str:
        return self.data['tokenizerCharclass']

    @property
    def languages(self) -> Optional[List[CqpLanguage]]:
        return self.data['languages']

    @property
    def best_lemma(self) -> dict:
        return self.data['bestLemma']
    pass
