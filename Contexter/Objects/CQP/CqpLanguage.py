class CqpLanguage:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def probality(self) -> float:
        return self.data['probability']

    @property
    def language(self) -> str:
        return self.data['language']
    pass
