class CqpQuerySite:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def score(self) -> float:
        return self.data['score']

    @property
    def url(self) -> str:
        return self.data['url']
    pass
