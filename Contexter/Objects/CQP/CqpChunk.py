from typing import List


class CqpChunk:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def aliases(self) -> List[str]:
        return self.data['aliases']

    @property
    def type(self) -> str:
        return self.data['type']

    @property
    def subtype(self) -> str:
        return self.data['subtype']

    @property
    def source(self) -> str:
        return self.data['source']
    pass
