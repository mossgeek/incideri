from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.CQP.CqpMeaning import CqpMeaning


class Cqp:
    def __init__(self, data: dict):
        self.data = data['data']['cqp']
        self.data['meanings'] = listdict_to_object(self.data['meanings'], CqpMeaning)
        pass

    @property
    def correction(self) -> str:
        return self.data['correction']['correctedQuery']

    @property
    def meanings(self) -> List[CqpMeaning]:
        return self.data['meanings']
    pass
