from typing import List

from Contexter import listdict_to_object
from Contexter.Objects.CQP.CqpToken import CqpToken
from Contexter.Objects.CQP.CqpQuerySite import CqpQuerySite
from Contexter.Objects.CQP.CqpChunk import CqpChunk


class CqpQuery:
    def __init__(self, data: dict):
        self.data = data
        self.data['tokens'] = listdict_to_object(self.data['tokens'], CqpToken)
        self.data['chunks'] = listdict_to_object(self.data['chunks'], CqpChunk)
        self.data['querysites'] = listdict_to_object(self.data['querysites']['sites'], CqpQuerySite)
        pass

    @property
    def text(self) -> str:
        return self.data['text']

    @property
    def tokens(self) -> List[CqpToken]:
        return self.data['tokens']

    @property
    def chunks(self) -> List[CqpChunk]:
        return self.data['chunks']

    @property
    def querysites(self) -> List[CqpQuerySite]:
        return self.data['querysites']
    pass
