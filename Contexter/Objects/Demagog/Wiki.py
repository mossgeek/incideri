class Wiki:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def url(self) -> str:
        return self.data['title']

    @property
    def description(self) -> str:
        return self.data['description']
    pass
