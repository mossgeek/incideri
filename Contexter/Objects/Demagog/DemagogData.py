class DemagogData:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def face(self) -> str:
        return self.data['face']

    @property
    def misleading(self) -> int:
        return self.data['misleading']

    @property
    def truth(self) -> int:
        return self.data['truth']

    @property
    def untruth(self) -> int:
        return self.data['untruth']

    @property
    def unverifiable(self) -> int:
        return self.data['unverifiable']

    @property
    def url(self) -> str:
        return self.data['url']

    @property
    def party(self) -> str:
        return self.data['party']
    pass
