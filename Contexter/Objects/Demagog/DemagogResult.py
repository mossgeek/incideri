from Contexter.Objects.Demagog.DemagogData import DemagogData
from Contexter.Objects.Demagog import Wiki


class DemagogResult:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def wiki(self) -> Wiki:
        return Wiki(self.data['wiki'])

    @property
    def demagog(self) -> DemagogData:
        return DemagogData(self.data['demagog'])
    pass
