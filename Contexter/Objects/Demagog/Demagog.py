from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Demagog.DemagogResult import DemagogResult


class Demagog:
    def __init__(self, data: dict):
        self.data = listdict_to_object(data['data']['demagog'], DemagogResult)
        pass

    @property
    def demagog_results(self) -> List[DemagogResult]:
        return self.data
    pass
