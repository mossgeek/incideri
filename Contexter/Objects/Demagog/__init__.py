from .Demagog import Demagog
from .DemagogResult import DemagogResult
from .DemagogData import DemagogData
from .Wiki import Wiki
