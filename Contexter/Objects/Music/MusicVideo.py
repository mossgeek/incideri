class MusicVideo:
    def __init__(self, data: dict):
        self.data = data
        pass

    def id(self) -> str:
        return self.data['id']

    def title(self) -> str:
        return self.data['title']

    def description(self) -> str:
        return self.data['description']

    def thumbnail(self) -> str:
        return self.data['thumbnail']
    pass
