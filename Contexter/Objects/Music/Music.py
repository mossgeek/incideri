from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Music.MusicVideo import MusicVideo


class Music:
    def __init__(self, data: dict):
        self.data = data['data']['music']
        self.data['releatedList'] = listdict_to_object(self.data['releatedList'], MusicVideo)
        pass

    def title(self) -> str:
        return self.data['title']

    @property
    def description(self) -> str:
        return self.data['description']

    @property
    def song_name(self) -> str:
        return self.data['songNAme']

    @property
    def interpret_name(self) -> str:
        return self.data['interpretName']

    @property
    def video_id(self) -> str:
        return self.data['videoId']

    @property
    def video(self) -> MusicVideo:
        return MusicVideo(self.data['video'])

    @property
    def releated_list(self) -> List[MusicVideo]:
        return self.data['releatedList']
    pass
