class CelebInfo:
    def __init__(self, data: dict):
        self.data = data['data']['celeb']
        pass

    @property
    def type(self) -> str:
        return self.data['type']

    @property
    def name(self) -> str:
        return self.data['name']

    @property
    def entity(self) -> str:
        return self.data['entity']

    @property
    def uri(self) -> str:
        return self.data['uri']

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def extarct(self) -> str:
        return self.data['extract']

    @property
    def details(self) -> str:
        return self.data['details']  # Je to JSON, ale liší se dle typu entity. Prozatím žádný objekt. Bylo by třeba na základě typu.
    pass
