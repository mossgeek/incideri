from Contexter.__base__ import caller_sug


class Suggest:
    def __init__(self, query):
        self.data = caller_sug(query, count=6)
        pass

    @property
    def results(self):
        l = []
        for i in self.data['result']:
            l.append(i['sentence'])
            pass
        return l
    pass
