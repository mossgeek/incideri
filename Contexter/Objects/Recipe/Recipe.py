from typing import List
from Contexter import listdict_to_object
from Contexter.Objects.Recipe.RecipeResult import RecipeResult


class Recipe:
    def __init__(self, data: dict):
        self.data = listdict_to_object(data['data']['recipe'], RecipeResult)
        pass

    @property
    def recipes(self) -> List[RecipeResult]:
        return self.data
    pass
