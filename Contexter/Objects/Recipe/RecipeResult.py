from typing import List


class RecipeResult:
    def __init__(self, data: dict):
        self.data = data
        pass

    @property
    def title(self) -> str:
        return self.data['title']

    @property
    def web_url(self) -> str:
        return self.data['webUrl']

    @property
    def thumb_url(self) -> str:
        return self.data['thumbUrl']

    @property
    def preparation_time(self) -> int:
        return self.data['preparationTime']

    @property
    def directions(self) -> List[str]:
        return self.data['directions']
    pass
