from Contexter import organic_caller
from Contexter.Objects import Organic
from Contexter.Objects import Suggest
from Contexter.Matrix import Matrix
import networkx


class Siter:
    # @staticmethod
    # def cqp_siter(self, query):
    #     sug = Suggest()
    #
    #     pass

    @staticmethod
    def org_siter(query: str, org: Organic):
        queries = (Suggest(query)).results
        # queries.append(query)

        if query in queries:
            queries.pop(queries.index(query))

        data = {}
        for q in queries:
            sites = []
            for doc in (Organic(organic_caller(q, 10))).organic_documents:
                sites.append(doc.snippet.url)
                pass

            data[q] = sites

            pass
        return Matrix(data)

    @staticmethod
    def matrix_to_context(pairs, solo):
        d = []
        for p in pairs:
            d.append(list(p))
            pass

        d.append(list(solo))

        return d

    @staticmethod
    def concatenator(pairs, solo):
        g = networkx.Graph(pairs)
        topics = list(networkx.find_cliques(g))

        if len(topics) > 1:
            if set(topics[0]) & set(topics[1]):
                tmp = list(set(topics[0]) | set(topics[1]))
                topics = [tmp]
                pass
            pass

        for item in list(solo):
            l = [item]
            topics.append(l)
            pass
        return topics
    pass
