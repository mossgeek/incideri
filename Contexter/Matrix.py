def Matrix(data: dict):
    for q in data:
        for i, dom in enumerate(data[q]):
            data[q][i] = dom.replace('//', '/').split('/')[1]
            pass
        pass

    matching = []
    match_list = set([])
    all = set([])
    for q in data:
        all.add(q)
        for qq in data:
            if q == qq:
                continue

            if len(set(data[q]).intersection(set(data[qq]))) > 1:
                match_list.add(q)
                pair = [q, qq]
                pair.sort()
                if pair not in matching:
                    matching.append(tuple(pair))
            pass
        pass

    single = (all - match_list)
    return matching, single
