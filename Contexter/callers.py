from .__base__ import caller_graphql


def cqp_caller(query):
    schema = """
    query Test($q: String!)
{ cqp(query:$q)
  {
		correction {
		  correctedQuery
		},
    meanings {
      isPorn,
      queries {
        text,
        tokens {
          text
          tokenizerCharclass
          languages {language, probability},
          bestLemma {
            lemma
          }
        },
        chunks {
          type
          subtype
          source
          aliases {
            text
          }
        },
        querysites {
          sites {
            score
            url
          }
        }
      },
      languages {
        language
        probability
      }
    }
  }
}"""
    return caller_graphql(schema, variables={'q': query})


def organic_caller(query, limit):
    schema = """
query Test($q: String!, $limit: Int) {
  organic(query: $q, limit: $limit) {
    docId,
    snippet{
      url,
      description,
      title
    }
  }
}"""
    return caller_graphql(schema, variables={'q': query, 'limit': limit})


def weather_caller(query):
    schema = """
query Test($q: String!) {
  weather(query: $q) {
    locality
    requestType
    results {
      locality {
        accusative
        regionName
        localityLongitude
        localityLatitude
        locative
        name
        type
        url
        photoLive
      }
      morning {
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
      afternoon{
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
      night{
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
      actual{
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
      evening{
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
      nextdaysList{
        windSpeed
        bio
        tempNight
        description
        windDirection
        weatherId
        humidity
        weatherIdDesc
        tempDay
        cloudiness
        date
        temp
        isNight
        img
      }
    }
  }
}"""
    return caller_graphql(schema, variables={'q': query})


def news_caller(query, limit, offset, queryCorrections):
    schema = """
query Test($q: String!, $limit: Int, $offset: Int, $queryCorrection: Boolean) {
  news(query: $q, limit: $limit, offset: $offset, queryCorrection: $queryCorrection) {
      docId
    	snippet{
        url
        description
        title
        urlHighlighted
      }
    	attributes{
        fileType
        isPorn
        isHomepage
        language
        lastChangeDate
        rssPublicationDate
      }
  }
}
"""
    return caller_graphql(schema, variables={'q': query, 'limit': limit, 'offset': offset, 'queryCorrection': queryCorrections})


def recipe_caller(query):
    schema = """
query Test($q: String!) {
  recipe(query: $q) {
   	title
    webUrl
    thumbUrl
    preparationTime
    directions
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def music_caller(query):
    schema = """
query Test($q: String!) {
  music(query: $q) {
   	title
    description
    songName
    interpretName
    videoId
    video {
      id
    }
    relatedList {
      id
    }
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def celeb_caller(query):
    schema = """
query Test($q: String!) {
  celeb(query: $q) {
   	type
    name
    entity
    uri
    title
    extract
    details
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def zoo_caller(query):
    schema = """
query Test($q: String!) {
  zoo(query: $q) {
   	type
    uri
    title
    extract
    details
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def map_caller(query):
    schema = """
query Test($q: String!) {
  maps(query: $q) {
   	title
    url
    mark{
      easting
      lat
      lon
      northing
    }
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def demagog_caller(query):
    schema = """
query Test($q: String!) {
  demagog(query: $q) {
  	wiki {
  	  title
  	  url
  	  description
  	}
    demagog {
      face
      misleading
      truth
      untruth
      unverifiable
      url
      party
    }
  }
}
"""
    return caller_graphql(schema, variables={'q': query})


def live_queries_caller(limit):
    schema = """
query Test($limit: Int) {
  live_queries(limit: $limit)
}
"""
    return caller_graphql(schema, variables={'limit': limit})


def live_news_caller(limit):
    schema = """
query Test($limit: Int) {
  live_news(limit: $limit){
    url
    title
    perex
    publicationTimestamp
  }
}
"""
    return caller_graphql(schema, variables={'limit': limit})
