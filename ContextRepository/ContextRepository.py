from ContextRepository.ContextEntityList import ContextEntityList


class ContextRepository:
    def __init__(self, repository: list):
        self.repository = []
        for rep in repository:
            self.repository.append(ContextEntityList(rep))
        pass

    def getRepository(self):
        return self.repository

    def getNumberOfContextEntityLists(self):
        return len(self.repository)
