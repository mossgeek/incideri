from ContextRepository.ContextEntity import ContextEntity


# List of context entities
class ContextEntityList:
    def __init__(self, entities: list):
        self.entities = []
        for e in entities:
            self.entities.append(ContextEntity(e))
        pass

    def getEntities(self):
        return self.entities
